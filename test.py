#!/usr/bin/env python

from __future__ import print_function

import traceback
import sys

from quiet_None import quiet_None

with quiet_None():
    print ('Context manager working, as None() returns', None())

@quiet_None()
def f():
    print ('Decorator working, as None() returns', None())

f()

try:
    print ("Now let's see whether we can throw and catch exceptions:")
    print ('bar', None())
except Exception:
    print ('Exception caught as should be')
