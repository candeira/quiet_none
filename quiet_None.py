#/usr/bin/env python

import ctypes
import sys
import platform
import functools

# 0. This is just so the context manager can also be used as a decorator

class ContextDecorator(object):
    def __call__(self, f):
        @functools.wraps(f)
        def decorated(*args, **kwds):
            with self:
                return f(*args, **kwds)
        return decorated

# 1. First, the context decorator that will determine 
# activation of quiet_None vs the usual 'signaling None'

_frames_using_quiet_None = set()
        
class quiet_None(ContextDecorator):
    def __enter__(self):
        _frames_using_quiet_None.add(sys._getframe(1))
    def __exit__(self, etype, evalue, traceback):
        _frames_using_quiet_None.remove(sys._getframe(1))
        return False

# 2. We need to know whether we are in a quiet_None context
# so we walk up the stack of current and ancestor frames 
# looking for an activated frame

def is_quiet_None_active():
    frame = sys._getframe()
    while frame:
        if frame in _frames_using_quiet_None:
            return True
        frame = frame.f_back
    return False

# 3. Last part is monkeypatching None with code that recognises activation

# stolen from Richard Jones
# typedef PyObject * (*ternaryfunc)(PyObject *, Pyobject *, Pyobject *); the
# third (keyword) argument is NULL'able and ctypes won't just convert that to
# a None for us, so we have to do it manually
ternaryfunc = ctypes.CFUNCTYPE(
    ctypes.py_object, ctypes.py_object, ctypes.py_object, ctypes.c_void_p)
ternaryfunc_p = ctypes.POINTER(ternaryfunc)

Py_ssize_t = ctypes.c_int64  if platform.architecture()[0] == '64bit' \
        else ctypes.c_int

@ternaryfunc
def none_call(obj, arg, kw):
    if kw:
        kw = ctypes.cast(kw, ctypes.py_object).value
    else:
        kw = {}
    if is_quiet_None_active():
        return None
    else:
        raise TypeError("'NoneType' object is not callable")

class PyTypeObject(ctypes.Structure):
    _fields_ = (
        ("ob_refcnt", ctypes.c_int),
        ("ob_type", ctypes.c_void_p),
        ("ob_size", ctypes.c_int),
        ("tp_name", ctypes.c_char_p),
        ("tp_basicsize", Py_ssize_t),
        ("tp_itemsize", Py_ssize_t),
        ("tp_dealloc", ctypes.c_void_p),        # destructor),
        ("tp_print", ctypes.c_void_p),          # printfunc),
        ("tp_getattr", ctypes.c_void_p),        # getattrfunc),        <<<
        ("tp_setattr", ctypes.c_void_p),        # setattrfunc),
        ("tp_reserved", ctypes.c_void_p),       
        ("tp_repr", ctypes.c_void_p),           # reprfunc),
        ("tp_as_number", ctypes.c_void_p),      # PyNumberMethods),
        ("tp_as_sequence", ctypes.c_void_p),    # PySequenceMethods),  <<<
        ("tp_as_mapping", ctypes.c_void_p),     # PyMappingMethods),   <<<
        ("tp_hash", ctypes.c_void_p),           # hashfunc),
        ("tp_call", ternaryfunc),               # ternaryfunc),        <<< why not ternaryfnc_p?
        ("tp_str", ctypes.c_void_p),            # reprfunc),
    )

class PyObject(ctypes.Structure):
    _fields_ = (
        ("ob_refcnt", ctypes.c_int),
        ("ob_type", ctypes.POINTER(PyTypeObject)),
    )

def patch():
    ctypes_mod = PyObject.from_address(id(None))
    ctypes_mod_ob_type = ctypes_mod.ob_type.contents
    ctypes_mod_ob_type.tp_call = none_call

patch()
