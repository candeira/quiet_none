
## What?

`quiet_None` is an exercise in implementing PEP336, also known as "Make None Callable", also known as "the worst PEP ever".

## What again?

This project makes Python's None a callable. You can call it like you would expect: `None()`. This will return `None`.

## But... How?

* Using CTypes to open up the NoneType object to add the `__call__()` method.
* Using `sys.get_frame()` to keep track of whether `quiet_None` is activated or not.
* Wrapping the `quiet_None` activation in a context manager/decorator so activation can be turned on and off, because I'm not an *animal*.

It all comes together like this:

```
kandinski@desire:~/data/work/hack/quiet_None$ python3
Python 3.4.0 (default, Apr 11 2014, 13:05:11) 
[GCC 4.8.2] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
>>> from quiet_None import quiet_None
>>> 
>>> with quiet_None():
...     print(None())
... 
None
>>> 
>>> @quiet_None()
... def f(x):
...     print(x())
... 
>>> f(None)
None
```

## I mean, Why?

I'm tired of constantly having to write guards around property accesses in data munging pipelines, and envious of the Maybe monads and Option types in other languages. So I had an itch to scratch.

Then I was told that Callable None should not be done, and it couldn't be done as a loadable module anyway so, well, I just HAD to.

## Thanks

* Nick Coghlan for bringing PEP336 to my attention and suggesting the `quiet_None` name.
* Richard Jones for his CTypes tutorial in "Don't Do This".
* Ryan F Kelly for his pointers on stack frame manipulation. 
* Graeme Cross and the MPUG community for their encouragement.

If any of this enrages you, please don't shout at them. It's all my fault.
